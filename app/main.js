//import node modules
const HTTP = require('http');
const URL = require("url");
const FS = require("fs");

// import custom modules
const DT = require("./firstmodule");

console.log("coucou nous sommes le : " + DT.myDT());

console.log(DT.myTutu());


HTTP.createServer(function (req, res) {
    // lire url
/*  res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write(req.url); 
    res.end() */


    // lire date in url ?/year=1582&month=12
/*  res.writeHead(200, {'Content-Type': 'text/html'});
    let q = URL.parse(req.url, true).query;
    let txt = q.year + ' / ' + q.month;
    res.write("<h1>" + txt + "</h1>");
    res.end();
 */

    // copie le contenu d'un fichier dans data
    FS.readFile("demofile.html", (err, data) => {
        res.writeHead(200, {'Content-Type': 'text/html'});
        //res.write(data);
        res.end(data);
    })

    // crée un fichier et ajoute un content
    FS.appendFile('mynewfile1.txt', 'Hello content!', function (err) {
        if (err) throw err;
        console.log('Saved!')
        res.end();
    })

    // suppr le fichier, on doit gérer si elle existe sinon le serveur shut down
    FS.unlink('mynewfile1.txt', function(err) {
        if (err) throw err;
        console.log('File deleted!')
        res.end();
    })



}).listen(666); 