const fs = require("fs");
const EXPRESS = require("express");
const parkings = require("./parking.json");
const app = EXPRESS();
const port = 666;

app.use(EXPRESS.json());

app.get("/", (req, res) => {
    res.send("HOME PAGE : nothing to do here");
});

app.get("/info", (req,res) => {
    res.writeHead(200, {'Content-Type' : 'application/json'});
    res.end(fs.readFileSync("package.json"));
})

app.get("/parkings", (req, res) => {
    res.status(200).json(parkings);
})

app.get("/parkings/:id", (req, res) => {
    const id = parseInt(req.params.id);
    const parking = parkings.find(parking => parking.id === id);
    if (parking) res.status(200).json(parking);
    else res.status(404).end;
})

app.post('/parkings', (req, res) => {
    // utiliser les fs si pour réactuliser le fichier
    let obj = JSON.parse(fs.readFileSync("parking.json"));
    obj.push(req.body);
    let newParking = JSON.stringify(obj);
    fs.writeFileSync("parking.json", newParking);
    res.status(200).end;
});

app.put('/parkings/:id', (req, res) => {
    // utiliser les fs si pour réactuliser le fichier
    const id = parseInt(req.params.id);
    const parking = parkings.find(parking => parking.id === id);
    parking.name = req.body.name;
    parking.city = req.body.city;
    parking.type = req.body.type;
    res.status(200).json(parkings);
})

app.delete('/parkings/:id', (req,res) => {
    // utiliser les fs si pour réactuliser le fichier
    const id = parseInt(req.params.id);
    const parking = parkings.find(parking => parking.id === id);
    parkings.splice(parkings.indexOf(parking),1);
    res.status(200).json(parkings);
})

app.listen(666, () => {
    console.log("running at : " + port);
});