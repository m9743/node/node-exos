// insertMany()

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://172.17.0.2:27017/";
/*
MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("mydb");
  var myobj = [
    { name: 'John', address: 'Highway 71'},
    { name: 'Peter', address: 'Lowstreet 4'},
    { name: 'Amy', address: 'Apple st 652'},
    { name: 'Hannah', address: 'Mountain 21'},
    { name: 'Michael', address: 'Valley 345'},
    { name: 'Sandy', address: 'Ocean blvd 2'},
    { name: 'Betty', address: 'Green Grass 1'},
    { name: 'Richard', address: 'Sky st 331'},
    { name: 'Susan', address: 'One way 98'},
    { name: 'Vicky', address: 'Yellow Garden 2'},
    { name: 'Ben', address: 'Park Lane 38'},
    { name: 'William', address: 'Central st 954'},
    { name: 'Chuck', address: 'Main Road 989'},
    { name: 'Viola', address: 'Sideway 1633'}
  ];
  dbo.collection("customers").insertMany(myobj, function(err, res) {
    if (err) throw err;
    console.log("Number of documents inserted: " + res.insertedCount);
    db.close();
  });
});


MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("mydb");
  var myobj = [
    { _id: 154, name: 'Chocolate Heaven'},
    { _id: 155, name: 'Tasty Lemon'},
    { _id: 156, name: 'Vanilla Dream'}
  ];
  dbo.collection("products").insertMany(myobj, function(err, res) {
    if (err) throw err;
    console.log(res);
    db.close();
  });
});
 */


// findOne ne ramène que le premier enregistrement :
MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("hopital");
  dbo.collection("employe").findOne({}, function(err, result) {
    if (err) throw err;
    console.log(result.name);
    db.close();
  });
});

// ramène tous le contenu d'une collection
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("hopital");
    dbo.collection("employe").find({}).toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
        db.close();
    });
});

// projection : (0 masqué et 1 visible)
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("hopital");
    dbo.collection("employe").find({}, { projection: { _id: 0} }).toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
        db.close();
    });
});

// requête avec un filtre (comme un WHERE en SQL)
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("hopital");
    var query = { phone: "04.96.03" };
    dbo.collection("employe").find(query).toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
        db.close();
    });
});

// // requête filtre avec regex (comme un LIKE en SQL)
// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     var dbo = db.db("mydb");
//     var query = { address: /^S/ };
//     dbo.collection("customers").find(query).toArray(function(err, result) {
//         if (err) throw err;
//         console.log(result);
//         db.close();
//     });
// });

// // 1 ordre alphabétique et -1 ordre inverse
// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     var dbo = db.db("mydb");
//     var mysort = { name: 1 };
//     dbo.collection("customers").find().sort(mysort).toArray(function(err, result) {
//         if (err) throw err;
//         console.log(result);
//         db.close();
//     });
// });

// // enlève champs correspondant à un critère
// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     var dbo = db.db("mydb");
//     var myquery = { address: 'Mountain 21' };
//     dbo.collection("customers").deleteOne(myquery, function(err, obj) {
//         if (err) throw err;
//         console.log("1 document deleted");
//         db.close();
//     });
// });

// // enlève champs à plusieurs critères 
// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     var dbo = db.db("mydb");
//     var myquery = { address: /^O/ };
//     dbo.collection("customers").deleteMany(myquery, function(err, obj) {
//         if (err) throw err;
//         console.log(obj.result + " document(s) deleted");
//         db.close();
//     });
// });

// // update 1 champs
// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     var dbo = db.db("mydb");
//     var myquery = { address: "Valley 345" };
//     var newvalues = { $set: {name: "Mickey", address: "Canyon 123" } };
//     dbo.collection("customers").updateOne(myquery, newvalues, function(err, res) {
//         if (err) throw err;
//         console.log("1 document updated");
//         db.close();
//     });
// });

// // update plusieurs champs
// // $set est un mot clé de MongoDB
// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     var dbo = db.db("mydb");
//     var myquery = { address: /^S/ };
//     var newvalues = {$set: {name: "Minnie"} };
//     dbo.collection("customers").updateMany(myquery, newvalues, function(err, res) {
//         if (err) throw err;
//         console.log(res.result + " document(s) updated");
//         db.close();
//     });
// });

// // supprime une collection
// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     var dbo = db.db("mydb");
//     dbo.collection("customers").drop(function(err, delOK) {
//         if (err) throw err;
//         if (delOK) console.log("Collection deleted");
//         db.close();
//     });
// });

// // limite le nombre de résultats
// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     var dbo = db.db("mydb");
//     dbo.collection("customers").find().limit(5).toArray(function(err, result) {
//         if (err) throw err;
//         console.log(result);
//         db.close();
//     });
// });

// /* orders
// [
//   { _id: 1, product_id: 154, status: 1 }
// ]
// products
// [
//   { _id: 154, name: 'Chocolate Heaven' },
//   { _id: 155, name: 'Tasty Lemons' },
//   { _id: 156, name: 'Vanilla Dreams' }
// ] */
// // faire un agrégat de 2 collections (comme une insertion d'une collection dans une autre avec ce qui correspont)
// // comme un JOIN avec une mécanique différente
// // $lookup est un mot clé de MongoDB
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("hopital");
    dbo.collection('employe').aggregate([
        { $lookup:
            {
            from: 'doctor',
            localField: 'speciality',
            foreignField: 'job',
            as: 'orderdetails'
            }
        }
    ]).toArray(function(err, res) {
        if (err) throw err;
        console.log(JSON.stringify(res));
        db.close();
    });
});