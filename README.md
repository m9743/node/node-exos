docker inspect <container_name> pour savoir l'adresse ip d'un conteneur pour lié le host d'une BDD par exemple

faire attention de bien importer les modules nécessaires (http, url, fs, etc...)



+ mysql :
    - npm install mysql

    - dans le conteneur mysql : 
    mysql  -u root -p (entrer mdp)
    ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'my-secret-pw';
    FLUSH privileges ;
    EXIT;



+ express : 
    - npm init (crée un dossier contenant les modules node et le package.json pour les dépendences)
    - npm install express
    - GET /parkings/:id/reservations
    GET /parking/:id/reservations/:idReservation
    POST /parkings/:id/reservations
    PUT /parking/:id/reservations/:idReservation
    DELETE /parking/:id/reservations/:idReservation
    - installer Postman pour requêter et vérifier



+ sqlite3 :
    - npm install sqlite3
    - https://www.sqlite.org/docs.html



+ mongoDB :
    - npm install mongo
    - dans le conteneur MongoDB : mongo pour rentrer dans la db
    - utilisation de compass
    - CRUD pour mongoDB dans le fichier index
    - port de mongoDB : 27017


